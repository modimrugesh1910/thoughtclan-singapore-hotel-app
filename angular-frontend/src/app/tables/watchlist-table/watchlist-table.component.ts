import {
    AfterViewInit,
    Component,
    ElementRef,
    OnInit,
    ViewChild
} from '@angular/core';
import {animate, state, style, transition, trigger} from '@angular/animations';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
// import {exportTOExcel, getDiffColor} from '../../core/core.functions';
import {TableService} from '../tables.service';
import {fromEvent as observableFromEvent, BehaviorSubject} from 'rxjs';
import {distinctUntilChanged, debounceTime, switchMap} from 'rxjs/operators';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {CommonService} from '../../core/common.service';

@Component({
    selector: 'app-watchlist-table',
    templateUrl: './watchlist-table.component.html',
    styleUrls: ['./watchlist-table.component.scss'],
    animations: [
        trigger('detailExpand', [
            state('collapsed', style({height: '0px', minHeight: '0', display: 'none'})),
            state('expanded', style({height: '*'})),
            transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
        ]),
    ]
})

export class WatchlistTableComponent implements OnInit, AfterViewInit {
    dataSource: any;
    fundTableForm: FormGroup;
    headerNames: Array<string> = ['name', 'hostname', 'nbgname', 'nbname', 'lat', 'long', 'roomType', 'price'];
    columnsToDisplay: any;
    fundDataSource: any;
    selectedData: any;

    @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
    @ViewChild(MatSort, {static: false}) sort: MatSort;
    @ViewChild('filter', {static: false}) filter: ElementRef;

    constructor(private readonly tableService: TableService, private formBuilder: FormBuilder,
                private readonly commonService: CommonService) {
    }

    ngOnInit(): void {
        this.fundTableForm = this.formBuilder.group({
            fundSchemeName: new FormControl('', Validators.required)
        });
    }

    ngAfterViewInit(): void {
        this.tableService.fetchData().subscribe((res) => {
            res.map(function (item) {
                delete item._id;
                return item;
            });
            this.dataSource = new MatTableDataSource(res);
            this.columnsToDisplay = res;
            this.dataSource.paginator = this.paginator;
            this.dataSource.sort = this.sort;

            observableFromEvent(this.filter.nativeElement, 'keyup').pipe(
                debounceTime(150),
                distinctUntilChanged(),)
                .subscribe(() => {
                    if (!this.dataSource) {
                        return;
                    }
                    this.dataSource.filter = this.filter.nativeElement.value;
                });
        });
    }

    private getFundRawData({value, valid}: { value: any, valid: boolean }) {
        return this.fundTableForm.getRawValue();
    }

    applyFilter(filterValue: string) {
        this.dataSource.filter = filterValue.trim().toLowerCase();
    }

    getSelectedData(data) {
        this.selectedData = data;
    }
}

