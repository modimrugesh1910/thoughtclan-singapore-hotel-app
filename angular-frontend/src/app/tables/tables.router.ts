import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {WatchlistTableComponent} from './watchlist-table/watchlist-table.component';

const materialWidgetRoutes: Routes = [
  {path: '', component: WatchlistTableComponent, data: {animation: 'watchlist'}},
];

@NgModule({
  imports: [
    RouterModule.forChild(materialWidgetRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class TablesRouterModule {
}
