const EXPRESS_SERVER = 'http://localhost:8080/';

/** URL strings used in API calls */
export const URL_API = {
  /**
   * Login api
   */
  LOGIN: EXPRESS_SERVER + 'login',
  LOCAL_STORAGE: {
    KEYS: {
      TOKEN: 'access-token',
    }
  },
  DATA: function(): string {
    return EXPRESS_SERVER + 'api/hotelist/';
  }
};
