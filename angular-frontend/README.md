# Angular 8 with Angular Material 2

This is a demo project for Angular 8 with Angular Material 2.

## Development Build

1. `npm install`
2. `npm start`

## Production Build

1. `npm run build`

## contact details  

phone - 7990782930

## Live Demo
https://ecs-frontend-mrugesh.netlify.com/

## Github
git clone https://modimrugesh1910@bitbucket.org/modimrugesh1910/ecs-frontend-book-repo.git

## Activities Done - 

I have covered below details -

* fetching data over http call
* sharing data over files
* display data in sortable list of books with ratings displayed in star format
* store cart data in local storage
* Implemented cart feature
* sorting functionality in table
* searching functionality in table
* pagination in table
* Download data in csv format
* version control
* deployment of code
