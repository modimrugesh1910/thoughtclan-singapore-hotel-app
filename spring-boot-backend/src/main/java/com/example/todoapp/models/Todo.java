package com.example.todoapp.models;

import java.util.Date;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection="hotel")
@JsonIgnoreProperties(value = {"createdAt"}, allowGetters = true)
public class Todo {
    @Id
    private String id;

    @NotBlank
    @Size(max=100)
    @Indexed(unique=true)
    private String title;

    private Boolean completed = false;

    private Date createdAt = new Date();

    private String name;
    private String host_name;
    private String neighbourhood_group;
    private String neighbourhood;
    private String latitude;
    private String longitude;
    private String room_type;
    private String price;

    public Todo() {
        super();
    }

    public Todo(String title) {
        this.title = title;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getHostname() {
        return host_name;
    }

    public void setHostname(String host_name) {
        this.host_name = host_name;
    }

    public String getNBGName() {
        return neighbourhood_group;
    }

    public void setNGBName(String neighbourhood_group) {
        this.neighbourhood_group = neighbourhood_group;
    }

    public String getNBName() {
        return neighbourhood;
    }

    public void setNBName(String neighbourhood) {
        this.neighbourhood = neighbourhood;
    }

    public String getLat() {
        return latitude;
    }

    public void setLat(String latitude) {
        this.latitude = latitude;
    }

    public String getLong() {
        return longitude;
    }

    public void setLong(String longitude) {
        this.longitude = longitude;
    }

     public String getRoomType() {
        return room_type;
    }

    public void setRoomType(String room_type) {
        this.room_type = room_type;
    }

    public String getPrice() {
        return price;
    }

    public void setPricen(String price) {
        this.price = price;
    }

    public Boolean getCompleted() {
        return completed;
    }

    public void setCompleted(Boolean completed) {
        this.completed = completed;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    @Override
    public String toString() {
        return String.format(
                "Todo[name=%s, host_name='%s', neighbourhood_group='%s', neighbourhood='%s', latitude='%s', longitude='%s', room_type='%s', price='%d']",
                name, host_name, neighbourhood_group, neighbourhood, latitude, longitude, room_type, price);
    }
}
