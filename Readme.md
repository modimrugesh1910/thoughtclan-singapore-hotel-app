# ThoughtClan Singapore Hotel App

## Requirements

1. Java - 1.8.x
2. Maven - 3.x.x
3. MongoDB - 3.x.x
4. NodeJS - 10.x.x
5. Angular cli - 8.x.x

## Import data 
Import data into local system
If you have MongoDB compass you can directly import in database collection using import option

or using ubuntu/apple terminal -

mongoimport -d singapore_hotel -c hotel --type csv --file Singapore_Listings.csv --headerline

## Steps to Setup

**1. Clone the application OR download app code**

```bash
git clone https://modimrugesh1910@bitbucket.org/modimrugesh1910/thoughtclan-singapore-hotel-app.git
```

**2. Build and run the backend app using maven**

```bash
cd spring-boot-backend
mvn package
java -jar target/todoapp-1.0.0.jar
```

Alternatively, you can run the app without packaging it using -

```bash
mvn spring-boot:run
```

The backend server will start at <http://localhost:8080>.

**3. Run the frontend app using npm**

```bash
cd angular-frontend
npm install
```

```bash
npm start
```

Frontend server will run on <http://localhost:4200>

## Work Done - 

* Use of CSV file and dump it in the database.
* List all the Hotel listings on the front end.
* Implement functionality to search hotels by name
* Provide feature to sort articles by price.
* Implement Filter based on region.
* Zip all your source code, executables, screenshots and upload the folder.
